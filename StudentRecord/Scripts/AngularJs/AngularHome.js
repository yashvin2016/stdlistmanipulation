﻿/// <reference path="../views/pageLookUp.html" />
/// <reference path="../angular.min.js" />
/// <reference path="angular.min.js" />


var myApp = angular.module("homeModule", ['ngRoute']);

myApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider.
          when("/Home", { templateUrl: "Scripts/views/Home.html" })
          .when("/pageDelete", { templateUrl: "Scripts/views/pageDelete.html" })
           .when("/pageRegister", { templateUrl: "Scripts/views/pageRegister.html" })
            .when("/pageUpdate", { templateUrl: "Scripts/views/pageUpdate.html" })
            .when("/pageLookUp", { templateUrl: "Scripts/views/pageLookUp.html" })
          .otherwise({
              redirectTo: "/Home"
          })
    $locationProvider.html5Mode(true);
})



var myController = function ($scope) {
        $scope.message = "yashvin";
};
myApp.controller("myController", myController);


myApp.controller("deleteController", function ($scope, $http) {

    $scope.OnDeleteClick = function () {
        window.location = "#/pageDelete"
    }

    $http({
        method: 'GET',
        url: 'http://localhost:61386/api/stddetail'
    }).then(function successCallback(response) {
        var stdLst = response.data;
        $scope.stdLst = stdLst;
        console.log(response)
    }, function errorCallback(response) {
        alert("error");
    });

    $scope.btnDeleteStd = function (std) {
        var index = stdLst.indexOf(std);
        $scope.stdLst.splice(std, 1);
        console.log(index);
    }
});

myApp.controller("registerController", function ($scope,$http) {

    $scope.OnRegisterClick = function () {
        window.location = "#/pageRegister"
    }

    $http({
        method: 'GET',
        url: 'http://localhost:61386/api/stddetail'
    }).then(function successCallback(response) {
        var stdLst = response.data;
        $scope.stdLst = stdLst;
        console.log(response)
    }, function errorCallback(response) {
        alert("error");
    });

    $scope.btnAddStd = function () {

        var data = {FName:"test",MId:"test" };
        $http({
            method: 'POST',
            url: 'http://localhost:61386/api/stddetail',
            data: data,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });

      

    }
});

myApp.controller("updateController", function ($scope, $http) {

    $scope.OnUpdateClick = function () {
        window.location = "#/pageUpdate"
    }

    $http({
        method: 'GET',
        url: 'http://localhost:61386/api/stddetail'
    }).then(function successCallback(response) {
        var stdLst = response.data;
        $scope.stdLst = stdLst;
        console.log(response)
    }, function errorCallback(response) {
        alert("error");
    });

    $scope.btnEditStd = function (std) {
        $scope.std =  std;
    }

    $scope.btnUpdateStd = function(){
        alert("Updated");
    }
});

myApp.controller("lookupController", function ($scope,$http) {

    $scope.OnLookUpClick = function () {
        window.location = "#/pageLookUp"
    }
     $http({
         method: 'GET',
         url: 'http://localhost:61386/api/stddetail'
     }).then(function successCallback(response) {
         var stdLst = response.data;
         $scope.stdLst = stdLst;
         console.log(response)
     }, function errorCallback(response) {
         alert("error");
     });
});


